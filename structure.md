# Powershell

* Each module should be 10-15 minutes in length
* Each module will have a summary section

## 1. What is Powershell and Why Should I Care?

### History of Powershell
* Foundations on .NET
* Initially separate download
* Versions deployed with Windows

### The Future
* ... and now, Open Source! Linux / macOS distributions

## 2. The Basics

### Conventions
* Commands use 'verb'-'noun' Conventions
* Cover [common verb usage][verbs] - 'Get', 'Add', 'Move' etc

### Get-Command
* Executing ```Get-Command```; whoa, that's a lot of commands...

### Get-Help
* Show excuting with no parameters
* How about getting help on the '```Get-Command```' cmdlet?
* But what about examples? '```Get-Help Get-Command -Examples```'

### Alias
* Show output of 'ls' and 'dir' commands
* So are 'ls' and 'dir' cmdlets? No... '```Get-Command ls```'
* What is '```Get-ChildItem```' then? '```Get-Help Get-ChildItem```'
* Show aliases for common commands (ls, dir, cd, mv, cat)

### Tab Completion
* Long cmdlet names are a pain to type; complete with Tab

## 3. Objects

### Introducting the Object Pipeline
* Output of Get-ChildItem is text, right? Wrong...
* Show Get-ChildItem on specific file (```Get-ChildItem C:\Windows\System32\drivers\etc\hosts```)
* Okay, so what if we want to see the time that the file was created? '```Get-ChildItem C:\Windows\System32\drivers\etc\hosts | Select-Object CreationTime```'
* Where on earth did that data come from?

### Get-Member
* Explain that the output of ```Get-ChildItem``` is not text, it's an object. In this case, it's a .NET type called '```System.IO.FileInfo```'
* Show '```Get-ChildItem C:\Windows\System32\drivers\etc\hosts | Get-Member```'
* So there's a member called 'FullName' '```Get-ChildItem C:\Windows\System32\drivers\etc\hosts | Select-Object FullName```'

### Formatting Objects
* Show '```Get-ChildItem C:\Windows\System32\drivers\etc\hosts | Format-Table```'
* What about a list? '```Get-ChildItem C:\Windows\System32\drivers\etc\hosts | Format-List```#'
* Filtering what is shown: '```Get-ChildItem C:\Windows\System32\drivers\etc\hosts | Format-Table Name, LastWriteTime, CreationTime```'

### Converting Objects
* What if we want to select some properties from ```Get-ChildItem``` and use as JSON? '```Get-ChildItem C:\Windows\System32\drivers\etc\* | Select-Object FullName, LastWriteTimeUtc | ConvertTo-Json```'
* Or convert to an HTML document? '```Get-ChildItem C:\Windows\System32\drivers\etc\* | Select-Object FullName, LastWriteTimeUtc | ConvertTo-Html | Set-Content table.html```'

## 4. Variables

### Simple Assignment
* Assign a name to a local variable '```$name = "Ed"```'
* Show it can be retrieved '```$name```'
* Show it can be used in a formatted string '```"Hello {0}" -f $name```'
* Or even... '```"Hello $name"```'

### Types
* Assign numeric values '```$value = 5```'
* Show basic math with numeric values
* Show built in type coersion '```$date = "25 December" -As [DateTime]```'
* Show the type '```$date.GetType()```' - C# devs will find that strangely familiar...

### Strings
* Assigment as interpolated vs literal string '```$a = "fred"; "Hello $a"```' vs. '```$a = "fred"; 'Hello $a'```'

### Arrays
* Initialise empty array '```$arr = @()```'
* Adding to arrays '```$arr += 'foo'```'

### Associative Arrays
* Initialise empty associative array '```$arr = @{}```'
* Add to associative array by index '```$arr['foo'] = 'bar';```'
* Add to associative array by property '```$arr.baz = 'quux'```'

## 5. The 'where' clause and iterators - Part 1

### Where-Object
* Find files in folder that have changed in last week '```$date = (Get-Date).AddDays(-7); Get-ChildItem -Recurse | Where-Object { $_.LastWriteTime -gt $date }```'; one simple line, and so much to unpack
* '```Where-Object```' has an alias '```?```'
* What is '```$_```'? For an explanation we need a diversion through iterators

### For-Each
* Lets show our 12 times table: '```1..10 | ForEach-Object { "{0} * 12 = {1}" -f $_, ($_ * 12) }```'; more to break down...
* Try '```1..10```' on its own - what does that do?
* So what is this mysterious '```$_```' variable then? Introduce the idea of automatic variables - $_ might be familiar to programmers from Perl
* Another quick mini-diversion

### Format Strings
* '```-f```' operator
* '```{0}```', '```{1}```', ...'```{n}```' placeholders  
* Again, .NET developers will find this very familiar - it's the same as ```String.Format```

### $PSItem vs. $_
* Microsoft introduced $PSItem as a more readable automatic variable than $_ in Powershell 3.0
* Anecdotally it seems that $_ still more widely used
* Other automatic variables will be covered later

## 6. The 'where' clause and iterators - Part 2

### Exploring predicates
* Numeric operators; '```-eq```,'```-ne```', '```-lt```', '```-gt```'; '```1..20 -lt 7```'
* String equality: '```"abc" -eq "abc"```'
* Case sensitivity; 'i' and 'c' prefixes: '```"ABC" -ceq "abc"```'  
* Contains: '```"abc", "def", "ghi" -contains "def"```'

## 7. Functions
* Build function to find next Friday 13th
* Extend function to take '```-FromDate```' parameter
* Extend function to get multiple results

## 8. Modules
* Introduce '```.psm1```' files and the modules folder
* Rewrite our Find-Friday13th function as module
* Introduce module loading and resolution
* Introduce 3rd-party PsGet module

[verbs]: https://msdn.microsoft.com/en-us/library/ms714428(v=vs.85).aspx
